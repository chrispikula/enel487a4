/**
Author: Chris Pikula
ENEL 487 A4.
This is a small application that tests the CRC16 function, for War and Peace.
Interestingly enough to note, .get() gives different results if you use it to parse
the text file in linux, vs windows.  This wasted several hours to debug.
*/

#include <iostream>
#include <fstream>
#include <chrono>
#include "crc.h"
#include <string>
typedef std::chrono::high_resolution_clock Clock;

int main()
{
	uint8_t * message;
	message = new uint8_t [3300000];
	std::string message2 = "";
	std::string messagetemp = "";

	int nBytes = 0;
	crc CRC_Output = 0;
	
	std::ifstream inputFile;
	inputFile.open("wrnpc11.txt");
	while (!inputFile.eof())
	{
		message[nBytes] = inputFile.get();
		nBytes++;
	}


	//std::cerr << "Read File" << std::endl;
	inputFile.close();

	inputFile.open("wrnpc11.txt");
	while (!inputFile.eof())
	{
		message2 += inputFile.get();
	}
	//std::cerr << "Read File" << std::endl;
	inputFile.close();

	auto t1 = Clock::now();
	auto t2 = Clock::now();
	std::cout << "Delta t2-t1 of an empty loop: "
		<< std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()
		<< " nanoseconds" << std::endl;


	//TEST CRC SLOW
	auto t3 = Clock::now();
	CRC_Output = crcSlow(message, nBytes-1);
	auto t4 = Clock::now();
	std::cout << "Delta t4-t3 of crcSlow: "
		<< std::chrono::duration_cast<std::chrono::nanoseconds>(t4 - t3).count() - 
		std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()
		<< " nanoseconds" << std::endl;
	std::cout << "CRC slow Output was: \"" << CRC_Output <<"\"" << std::endl;

	//std::cerr << "Method 1 length = " << nBytes << std::endl;
	//std::cerr << "Method 2 length = " << message2.length() << std::endl;
	//confirming that both methods gave the same length of message.  They do.

	//TEST CRC FAST
	auto t5 = Clock::now();
	crcInit();
	CRC_Output = crcFast((unsigned char*)message2.c_str(), message2.length()-1);
	auto t6 = Clock::now();
	std::cout << "Delta t5-t6 of crcFast: "
		<< std::chrono::duration_cast<std::chrono::nanoseconds>(t6 - t5).count() -
		std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()
		<< " nanoseconds" << std::endl;
	std::cout << "CRC fast Output was: \"" << CRC_Output << "\"" << std::endl;

	//unit test code
	std::string testMessage = "Hello World";
	CRC_Output = crcSlow((unsigned char*)testMessage.c_str(), testMessage.length());
	std::cout << "\"Hello World\"'s Slow CRC is:" << CRC_Output << std::endl;
	CRC_Output = crcFast((unsigned char*)testMessage.c_str(), testMessage.length());
	std::cout << "\"Hello World\"'s Fast CRC is:" << CRC_Output << std::endl;

	return 0;


}
