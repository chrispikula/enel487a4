ENEL 487 Assignment 4
This is an assignment that runs CCITT checks on war and peace.
CRC slow runs in 178,636,433 ns, and CRC fast runs in 17,901,808 ns.
It also computes the test case of "Hello World", and lets the user see if they match.

Frustraiting to note, is that the .get() works differently on windows than in does on linux, so my windows CRC check of 0xCF40 doesn't match my Snoopy CRC check of 0xD9DE.

Currently, I can't access snoopy from off campus, so I'll have to finish the report in the morning.

I didn't find any CRC16 or CRC32 code in the article.

As this code is GPL'd, I would be able to use it in a project, as long as I cited it as such.